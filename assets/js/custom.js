$( document ).ready(function() {
    $(".diamond-shape").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    var mobyMenu = new Moby({
        menu       : $('#mobify'), // The menu that will be cloned
    	mobyTrigger: $('#moby-button'), // Button that will trigger the Moby menu to open
    });

});
